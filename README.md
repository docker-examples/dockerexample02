**Quellcode**
                                                                                                                                                
Zu finden im helloworld-Ordner. 

Besteht im Prinzip aus einer Main-Klasse welche ein mal "Hello World!" ausgibt.

Beispiel dient dazu zu zeigen, dass der Container nur so lange läuft, bis der Hauptprozess beendet ist (was nach Ausgabe des "Hello World!" der Fall ist).

**Docker**

Zu finden im docker-Ordner.

**Befehle**

Image bauen: `docker build -t dockerdemo/docker02:latest .` (im docker-Ordner)

Container starten: `docker run --rm dockerdemo/docker02` --> Wir sind im Container - nach Ausgabe von "Hello World!" ist der Container weg.

ODER

Container starten (Alternative): `docker run --rm -d dockerdemo/docker02` --> Container laeuft im Hintergrund und ist trotzdem direkt wieder weg.

Container beenden: entfaellt, da der Container sich sowieso selber direkt wieder beendet.